variable "name"     { }
variable "pgp_key"  { }
variable "region"   { }

provider "aws" {
	region = var.region
}

module "storage" {
  source = "./modules/storage"

  name      = var.name
  pgp_key   = var.pgp_key
}

module "application" {
  source = "./modules/application"

  name = var.name
}

module "manage" {
  source = "./modules/manage"

  name          = var.name
  region        = var.region
  bucket_name   = module.storage.bucket_name
  sns_topic_arn = module.application.sns_topic_arn
}

output "access_id"  { value = module.storage.access_id }
output "access_key" { value = module.storage.access_key }
