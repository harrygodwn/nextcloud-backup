### Nextcloud backup to S3

## Introduction

This repo has been created to hold the Terraform code required to:
*  Build AWS infrastructure for backing-up a Nextcloud instance running inside a FreeBSD jail (on FreeNAS, specifically)


Also included are the tcsh scripts required to send the backup from the server via [`s3cmd`](https://github.com/s3tools/s3cmd). Tcsh was used due to it being the default shell on FreeBSD; having no proir experience, the writer thought it a useful learning experience. Bash is likely a better alternative...


Files are compressed using tar and uploaded to S3 with a 60 day expiry in Infrequent Access mode, making storage slightly cheaper but more expensive to retrieve (as is suitable for backups). The upload process is set to make a backup every 30 days, meaning there should be two month's worth of data stored in the bucket at any one time.


Should the backup process fail, there is a CloudWatch alarm configured to trigger if the number of backup files stored drop below an average of 1.5. This is alarm linked to an SNS topic designed to send the alert via email. There is a slight caveat here in that the email subscription must be configured manually via the AWS console to start recieving email alerts; this is owing to a Terraform limitation.


The bucket itself has a lifecycle rule to transition any other objects uploaded from Standard mode into Infrequent Access mode after a period of 30 days. The bucket uses server-side encryption and has a full public access block, as well as standard HTTPS encryption in transit. My use-case doesn't require too much thought about encryption but others may want to make some changes to the server-side set-up in case of paranoia using AWS-owned keys. The IAM user created to upload files has permissions to `GetBucketLocation`, `PutObject` and to `PutObjectAcl` for the specific bucket created. The access credentials for this IAM user will be output to the terminal via encrypted PGP using the desired public key; decryption details below.


See [s3cmd](https://github.com/s3tools/s3cmd) for how to configure the tool and for more usage options.

## Prerequisites

You'll probably need:
* An AWS account
* Terraform installed on your laptop or computer
* `gpg` or equivalent PGP tool installed on your laptop or computer
* Nextcloud installed on your server
* `s3cmd` installed on your server

# Assumptions

* Nextcloud is already configured
* You already have an appropriate AWS admin user profile in `~/.aws/crendentials`, `aws-vault`, `env` or whichever authentication mechanism you use on your laptop or computer with full permissions for SNS, CloudWatch, S3 & IAM
* You're a Linux user (some steps may need modifying if not)
* You have root permissions on the Nextcloud server

## Set-up & configuration

1. Create a `terraform.tfvars` file at the root of this directory. The `terraform.tfvars` file requires the following contents:
```
name	= "project-name-hyphens-only"
region  = "your-aws-region-1"
# gpg --export "User Name" | base64
pgp_key = <<EOT
mQINBF3T/ZwBEADYW1k7cO4pO3bDuEeKqbnosl5djLDlp8E+SyhSVx1bCD8idCvrhIHOX0gnZ6EG
Z7TVBGTz1swEhyFdPWyy0kArxAJ2YeaoAh4nhH2GxfEc9jwe79Fp4Q5FfelJEMYNNcfe42iyvmK0
3GmTEuV4qWyK1fjhNDvruiHtjjcaXvq4s7XLgMLxn6bfKFus5akaj6V4pf+ODq0FvMS+LHK45DsX
...
...
OUh3zrVU6sLlhCj5uwAvP7ZWqASH7wXTYC3kh0j2RmxBuG35
EOT
```
The `name` variable will be used for your S3 bucket DNS prefix; as such it needs to be DNS-friendly (alphanumeric with single hyphen separators only). The `region` variable should be your desired AWS region; I used `eu-west-2`. The `pgp_key` must be a public PGP key in base64 exported from whichever PGP keychain you prefer; in this case I used `gpg` and exported my public key using the command `gpg --export "User Name" | base64"`, where `User Name` was the name used to generate the key. PGP key generation is outside the scope of this guide.

2. Initiate the Terraform config using the `terraform init` command. At this point you may encounter errors if some variables from your `terraform.tfvars` file are missing or incorrect, or you've made a change to the code that's invalid.

3. Apply the Terraform code using the `terraform apply` command. Confirm the changes you are about to make by typing `yes` in the terminal (after reviewing them, of course!).

4. The IAM user credentials required to upload files to the now-created bucket will have been output to the terminal. To decrypt the access key, the following command can be used: `terraform output access_key | base64 --decode | gpg --decrypt`.

5. Configure `s3cmd` using the `s3cmd --configure` command on your server and follow the steps. You'll need your newly created AWS user ID and key from the Terraform output as mentioned above.

6. Copy the script files to `/root` on your server and make them executable; `chmod u+x *.sh`.

7. Replicate the crontab file in your server's root user's crontab; `crontab -e`.

8. You may wish to run the backup script to test the functionality is as expected; `./backup.sh`.
