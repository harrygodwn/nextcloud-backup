#!/usr/bin/env tcsh

pkg install -y py38-s3cmd

# Remove any previous backups
rm /tmp/backups/*

set filename=`date +'%Y%m%d'.tgz`

cd /tmp/backups

# Compress data to /tmp/backups/YYYYmmdd.tgz
tar --exclude=/usr/local/www/nextcloud/data/__groupfolders/trash --exclude=/usr/local/www/nextcloud/data/__groupfolders/versions -czvf $filename /usr/local/www/nextcloud/data/__groupfolders

# Transfer to bucket (lifecycle policies in S3)
s3cmd put --acl-private --server-side-encryption --storage-class=STANDARD_IA --expiry-days=60 /tmp/backups/$filename s3://harrygodwin-nextcloud

# Cleanup
rm /tmp/backups/*

cd
