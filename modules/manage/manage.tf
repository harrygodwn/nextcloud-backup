#--------------------------------------------------------------
# This module creates all management resources
#--------------------------------------------------------------

variable "name"           { }
variable "region"         { }
variable "bucket_name"    { }
variable "sns_topic_arn"  { }

module "cloudwatch" {
  source = "./cloudwatch"

  name          = var.name
  region        = var.region
  bucket_name   = var.bucket_name
  sns_topic_arn = var.sns_topic_arn
}
