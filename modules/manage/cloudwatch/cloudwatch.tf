#--------------------------------------------------------------
# This module creates all resources necessary for Cloudwatch
#--------------------------------------------------------------

variable "name"           { }
variable "region"         { }
variable "bucket_name"    { }
variable "sns_topic_arn"  { }

resource "aws_cloudwatch_metric_alarm" "s3_backup_fail_alarm" {
  alarm_name          = "${var.name}_nextcloud_backup_fail"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = 1
  metric_name         = "NumberOfObjects"
  namespace           = "AWS/S3"
  period              = 86400 # 1 day required by metric
  statistic           = "Average"
  threshold           = 1.5 # Average at 1.5 in case one expires before another backup is made

  dimensions = {
    BucketName  = var.bucket_name
    StorageType = "AllStorageTypes"
  }

  alarm_actions             = [var.sns_topic_arn]
  insufficient_data_actions = [var.sns_topic_arn]
}