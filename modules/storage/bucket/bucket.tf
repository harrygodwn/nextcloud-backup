#--------------------------------------------------------------
# This module creates all resources necessary for a bucket
#--------------------------------------------------------------

variable "name"     { }
variable "pgp_key"  { }

resource "aws_s3_bucket" "bucket" {
  bucket        = "${var.name}-nextcloud"
  acl           = "private"

	lifecycle_rule {
		enabled = true

		transition {
			days 					= 30
			storage_class	= "STANDARD_IA"
		}

		#transition { # Glacier has 90-day minimum charge
		#	days					= 40
		#	storage_class	= "GLACIER"
		#}

		expiration {
			days = 60
		}
	}

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags = {
    Name  = "${var.name}_nextcloud_bucket"
    Owner = var.name
  }
}

resource "aws_s3_bucket_public_access_block" "bucket_block" {
  bucket = aws_s3_bucket.bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_iam_user" "iam_user" {
  name = "${var.name}_nextcloud_user"

  tags = {
    Name  = "${var.name}_nextcloud_user"
    Owner = var.name
  }
}

resource "aws_iam_user_policy" "iam_policy" {
  name = "${var.name}_nextcloud_user_policy"
  user = aws_iam_user.iam_user.name

  policy = <<EOF
{
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
      "s3:GetBucketLocation"
      ],
      "Resource": ["${aws_s3_bucket.bucket.arn}"]
    },
    {
      "Effect": "Allow",
      "Action": [
      "s3:PutObject",
      "s3:PutObjectAcl"
      ],
      "Resource": ["${aws_s3_bucket.bucket.arn}/*"]
    }
  ]
}
EOF
}

resource "aws_iam_access_key" "iam_key" {
  user    = aws_iam_user.iam_user.name
  pgp_key = var.pgp_key
}

output "bucket_name"  { value = aws_s3_bucket.bucket.id }
output "access_id"    { value = aws_iam_access_key.iam_key.id }
output "access_key"   { value = aws_iam_access_key.iam_key.encrypted_secret }
