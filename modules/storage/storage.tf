#--------------------------------------------------------------
# This module creates all storage resources
#--------------------------------------------------------------

variable "name"     { }
variable "pgp_key"  { }

module "bucket" {
  source = "./bucket"
  name      = var.name
  pgp_key   = var.pgp_key
}

output "bucket_name"  { value = module.bucket.bucket_name }
output "access_id"    { value = module.bucket.access_id }
output "access_key"   { value = module.bucket.access_key }
