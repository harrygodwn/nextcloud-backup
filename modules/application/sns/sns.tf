#--------------------------------------------------------------
# This module creates all resouces necessary for SNS
#--------------------------------------------------------------

variable "name"   { }

resource "aws_sns_topic" "topic" {
  name = "${var.name}_topic"

  tags = {
    Name = "${var.name}_topic"
    Owner = var.name
  }
}

# resource "aws_sns_topic_subscription" "subscription" { # Not currently supported
#   topic_arn = aws_sns_topic.topic.arn
#   protocol  = "email"
#   endpoint  = var.email
# }

output "sns_topic_arn" { value = aws_sns_topic.topic.arn }
