#--------------------------------------------------------------
# This module creates all application resources
#--------------------------------------------------------------

variable "name"   { }

module "sns" {
  source = "./sns"

  name = var.name
}

output "sns_topic_arn" { value = module.sns.sns_topic_arn }
